const express = require('express');
const fetch = require('node-fetch');

const app = express();
const port = 3000;


app.listen(port, () => {
  console.log(`Приложение доступно по адресу: http://localhost:${port}`);
});

app.use('/', express.static(__dirname + '/static'));

app.get('/weather', async function (request, response) {
  if (!request.query.city) {
    return response.sendStatus(400);
  }

  try {
    const { city } = request.query;
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${encodeURIComponent(
      city
    )}&lang=ru&units=metric&appid=185320d2bf4e1821d39f229d03955cf0`;

    const dataWeather = await fetch(url, {
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
    }).then((item) => {
      return item.json().then((item) => {
        return item;
      });
    });
    if (dataWeather.cod === '404') {
      return response.send(`<h1>Погода в городе ${city} не найдена!</h1>`);
    }
    const data = `
  <h1>Город: ${dataWeather.name}</h1>
  <h3>${dataWeather.weather[0].description}</h3>
  <h2>Температура: ${dataWeather.main.temp} &#176С</h2>
  <h2>Влажность: ${dataWeather.main.humidity} %</h2>
  <h2>Ветер: ${dataWeather.wind.speed} м/с</h2>`;
    return response.send(data);
  } catch (e) {
    console.info(e);
    return e;
  }
});
